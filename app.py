import os
from flask import Flask, abort, g
from flask import render_template, send_from_directory
from flask import redirect, url_for
from flask import request, session
import requests, split, json
from werkzeug.wsgi import SharedDataMiddleware
import validators
from psycopg2 import connect, extras

from flask_oauth import OAuth
from functools import wraps
from urllib2 import Request, urlopen, URLError


# You must configure these 3 values from Google APIs console
# https://code.google.com/apis/console
GOOGLE_CLIENT_ID = 'GOOGLE_CLIENT_ID'
GOOGLE_CLIENT_SECRET = 'GOOGLE_CLIENT_SECRET'
REDIRECT_URI = '/oauth2callback'  # one of the Redirect URIs from Google APIs console
SECRET_KEY = 'developpement'
DEBUG = True

app = Flask(__name__)

app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
    '/statics':  os.path.join(os.path.dirname(__file__), 'statics')
})


#app.config.from_object('app.default_settings')
app.config.from_pyfile('local_settings.cfg')


GOOGLE_CLIENT_ID = app.config['GOOGLE_CLIENT_ID']
GOOGLE_CLIENT_SECRET = app.config['GOOGLE_CLIENT_SECRET']
SECRET_KEY = app.config['SECRET_KEY']


app.debug = DEBUG
app.secret_key = SECRET_KEY
oauth = OAuth()
 
google = oauth.remote_app('google',
                          base_url='https://www.google.com/accounts/',
                          authorize_url='https://accounts.google.com/o/oauth2/auth',
                          request_token_url=None,
                          request_token_params={'scope': 'https://www.googleapis.com/auth/userinfo.email',
                                                'response_type': 'code'},
                          access_token_url='https://accounts.google.com/o/oauth2/token',
                          access_token_method='POST',
                          access_token_params={'grant_type': 'authorization_code'},
                          consumer_key=GOOGLE_CLIENT_ID,
                          consumer_secret=GOOGLE_CLIENT_SECRET)


def get_or_create_person(result):
    res = json.loads(result)
    person = request_pg('sql/detail_person.sql', (res["id"],))
    if person:
        write_pg('sql/update_person.sql', (res["name"], res["picture"], res["email"], person[0]['id']))
    else:
        write_pg('sql/create_person.sql', (res["id"], res["name"], res["picture"] , res["email"]))
    person = request_pg('sql/detail_person.sql', (res["id"],))
    session['personid']= person[0]['id']


def requires_auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
      access_token = session.get('access_token')
      if access_token is None:
          return redirect(url_for('login'))
      access_token = access_token[0]
      headers = {'Authorization': 'OAuth '+access_token}
      req = Request('https://www.googleapis.com/oauth2/v1/userinfo',
                    None, headers)
      try:
          res = urlopen(req)
          result = res.read()
      except URLError, e:
          if e.code == 401:
              # Unauthorized - bad token
              session.pop('access_token', None)
              return redirect(url_for('login'))
      get_or_create_person(result);
      return f(*args, **kwargs)
  return decorated

@app.route('/login')
def login():
    callback=url_for('authorized', _external=True)
    return google.authorize(callback=callback)

@app.route(REDIRECT_URI)
@google.authorized_handler
def authorized(resp):
    access_token = resp['access_token']
    session['access_token'] = access_token, ''
    return redirect(url_for('index'))

@google.tokengetter
def get_access_token():
    return session.get('access_token')

@app.before_request
def before_request():
    print 'opening connection'
    conn = connect(user= None, host= None, password= None, port= 5432, database='jtlbd')
    g.db = conn

@app.after_request
def after_request(response):
    if g.db is not None:
        print 'closing connection'
        g.db.close()
    return response

def write_pg(fd, params=None):
    with open(fd) as fd:
        try:
            postgresql = g.db #connect(user= None, host= None, password= None, port= 5432, database='jtlbd')
            with postgresql.cursor(
                    cursor_factory=extras.RealDictCursor) as cur:
                cur.execute(fd.read(), params)
                postgresql.commit()
        except Exception as e:
            postgresql.rollback()
            raise
    #postgresql.close()

def request_pg(fd, params=None):
    with open(fd) as fd:
        try:
            postgresql = g.db #connect(user= None, host= None, password= None, port= 5432, database='jtlbd')
            with postgresql.cursor(
                    cursor_factory=extras.RealDictCursor) as cur:
                cur.execute(fd.read(), params)
                cur = cur.fetchall()
                #postgresql.close()
                return cur
        except Exception as e:
            postgresql.rollback()
            raise
    #postgresql.close()

@app.route('/')
@requires_auth
def index():
    bets = request_pg('sql/bet_list.sql')
    return render_template('index.html', bets=bets)

def is_valid(form):
    if (form["name"] and (form["photoUrl"]=='' or validators.url(form["photoUrl"]))):
        return True
    else:
        return False

def is_valid_choice(form):
    if (form["label"]!=''):
        return True
    else:
        return False


@app.route("/addbet", methods=['GET', 'POST'])
@requires_auth
def addbet():
    if request.method=="GET":
        return render_template('addbet.html', personid=session.get('personid'))
    elif request.method=="POST":
        form =request.form # A form bound to the POST data
        if is_valid(form):  # All validation rules pass
            # save in database
            write_pg('sql/add_bet.sql', (form["name"], form["photoUrl"], form["description"], form["type"], form["modif"], session.get('personid')))
            bet = request_pg('sql/get_last_bet.sql')
            betid=bet[0]['id']
            return json.dumps({'success':True, 'redirect':'/', 'betid':bet[0]['id']}, 200, {'ContentType':'application/json'})
        else:
            form = request.form  # An unbound form
            return json.dumps({'success':True, 'HTTPRESPONSE':"error" }), 400, {'ContentType':'application/json'}

@app.route("/addchoice/<betid>", methods=['POST'])
@requires_auth
def addchoice(betid):
    bet = request_pg('sql/detail_bet.sql', (betid,))
    if  (bet[0]['personid'] == session.get('personid')) or bet[0]['modifiable']:
        form =request.form # A form bound to the POST data
        if is_valid_choice(form):  # All validation rules pass
            # save in database
            write_pg('sql/add_choice.sql', (form["label"], form["betid"], form["personid"]))
            choiceid = request_pg('sql/get_last_choice.sql')
            return json.dumps({'success':True, 'redirect':'/listPnj', 'choiceid':choiceid[0]['id']}), 200, {'ContentType':'application/json'}
        else:
            form = request.form  # An unbound form
            return json.dumps({'success':True}), 400, {'ContentType':'application/json'}
    else:
        return "vous n'avez pas le droit d'administrer ce paris"

@app.route("/removechoice/<choiceid>", methods=['POST'])
@requires_auth
def removechoice(choiceid):
    choice=request_pg('sql/detail_choice.sql', (choiceid,))
    bet = request_pg('sql/detail_bet.sql', (choice[0]['betid'],))
    if  bet[0]['personid'] == session.get('personid'):
        write_pg('sql/remove_choice.sql', (choiceid,))
        return json.dumps({'success':True, 'redirect':'/'}), 200, {'ContentType':'application/json'}
    else:
        return "vous n'avez pas le droit d'administrer ce paris"

@app.route("/votechoice/<choiceid>", methods=['POST'])
@requires_auth
def votechoice(choiceid):
    choice=request_pg('sql/detail_choice.sql', (choiceid,))
    if hasAlreadyVoted(choice[0]['betid']):
        return json.dumps({'success':False, 'redirect':'/'}), 400, {'ContentType':'application/json'}
    else:
        write_pg('sql/vote_choice.sql', (choiceid, session.get('personid')))
        return json.dumps({'success':True, 'redirect':'/'}), 200, {'ContentType':'application/json'}


def hasAlreadyVoted(betid):
    vote=request_pg('sql/get_vote.sql', (betid, session.get('personid')))
    if vote:
        return True
    else:
        return False

@app.route("/betdetail/<betId>")
@requires_auth
def betdetail( betId):
    bet=request_pg('sql/detail_bet.sql', (betId,))
    choicelist = request_pg('sql/list_choices.sql', (betId,))
    voted=hasAlreadyVoted(betId)
    hascreated = (bet[0]['personid'] == session.get('personid'))
    votelist = request_pg('sql/list_choices_and_vote.sql', (betId,))
    return render_template('betdetail.html', choices=choicelist, bet=bet[0], personid=session.get('personid'), votelist=votelist, voted=voted, hascreated=hascreated)

@app.route("/betadmin/<betId>", methods=['GET', 'POST'])
@requires_auth
def betadmin( betId):
    bet=request_pg('sql/detail_bet.sql', (betId,))
    choicelist = request_pg('sql/list_choices.sql', (betId,))
    if request.method=="GET":
        if bet[0]['personid'] == session.get('personid'):
            return render_template('betadmin.html', choices=choicelist, bet=bet[0], personid=session.get('personid'))
        else:
            return "vous n'avez pas le droit d'administrer ce paris"
    elif request.method=="POST":
        form =request.form # A form bound to the POST data
        if is_valid(form):  # All validation rules pass
            # save in database
            write_pg('sql/update_bet.sql', (form["name"], form["photoUrl"], form["description"], form["type"], form["modif"], betId))
            return json.dumps({'success':True, 'redirect':'/', 'betid':betId}, 200, {'ContentType':'application/json'})
        else:
            form = request.form  # An unbound form
            return json.dumps({'success':True, 'HTTPRESPONSE':"error" }), 400, {'ContentType':'application/json'}

if __name__ == "__main__":
    app.run(host="localhost")
