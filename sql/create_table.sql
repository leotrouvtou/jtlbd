
drop table if exists person cascade;
CREATE TABLE person
(
id SERIAL PRIMARY KEY,
uuid TEXT,
name VARCHAR(100),
email VARCHAR(100),
picture TEXT
);

drop table if exists bet cascade;
CREATE TABLE bet
(
id SERIAL PRIMARY KEY,
name VARCHAR(100),
photourl TEXT,
description TEXT,
choicetype TEXT,
personid integer,
FOREIGN KEY (personid) REFERENCES person(id),
modifiable bool
);


drop table if exists choice cascade;
CREATE TABLE choice
(
id SERIAL PRIMARY KEY,
label VARCHAR(100),
betid integer,
FOREIGN KEY (betid) REFERENCES bet(id),
personid integer,
FOREIGN KEY (personid) REFERENCES person(id)
);


drop table if exists vote cascade;
CREATE TABLE vote
(
id SERIAL PRIMARY KEY,
choiceid integer,
FOREIGN KEY (choiceid) REFERENCES choice(id),
personid integer,
FOREIGN KEY (personid) REFERENCES person(id)
);
