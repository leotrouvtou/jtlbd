// drop down the menu, and swap the icon to the close icon
function toggleMenu(){
  $(this).toggleClass('fa-bars');
  $(this).toggleClass('fa-times');
  $('nav').toggleClass('down');
  $('nav li a').removeClass('down');
  $('.search').removeClass('down');
  $('.fa-search').removeClass('fa-times');
}

//Make sure the menu icon behaves corectly when the menu is open
$('nav li a').click(function(){
    $('.menu').addClass('fa-bars');
    $('.menu').removeClass('fa-times');
    $('nav').toggleClass('down');
});

//show and hide the search bar, also make sure if the menu is open to hide the menu, and ensure the menu icon state is correct
function toggleResearch(){
    $(this).toggleClass('fa-times');
    $('.menu').addClass('fa-bars');
    $('.menu').removeClass('fa-times');
    $('.search').toggleClass('down');
    $('nav').removeClass('down');
}

function message(){
        $("#message").html("<strong>Oh snap! </strong> Change a few things up and try submitting again.");
        // show
        $("#message").show();
        $("#message").alert();
        // hide after 3 seconds
        var t = setTimeout("$(\"#message\").hide() ;",3000);
}
