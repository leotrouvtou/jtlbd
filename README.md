# Je te l'avais bien

Je te l'avais bien dit is an application that allow you to make bets
with friends

# Installation

## create a virtualenv :

```
mkvirtualenv jtlbd
workon jtlbd
```

## Install all requirements

```
pip install -r requirements.txt
```

## create database

```
psql
create database jtlbd;
\q
psql jtlbd <sql/create_table.sql
```

## create local_settings.cfg

## launch application

```
python app.py
```
